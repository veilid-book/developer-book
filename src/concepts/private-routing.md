# Private Routing

A private route allows apps on a Veilid network to create an endpoint that the consuming app can send to, but provides no other information.  This differs from normal Veilid operation by hiding the address of the route creator during real time communication.

Private routes differ from safety routes.  A safety route is a contextual flag on a routing context, designed to protect the sender. The developer using the context must know that communication using the context will protect the identity of the local node. As such, the flag just tells the context that when it needs to route messages, it should construct and use safety routes for them rather than directly routing them. 

Private routes are created on request by the application,and are designed to protect the receiver. A private route is requested by the application when it wants to receive messages but still have its identity protected. Because the app needs to maintain it, it's much less of a transparent process.

The process for private route use is more or less:

1. Get a private blob for an endpoint from the DHT.
2. Import the private route blob and get a route ID.
 * If there is an error, the route is not present, so wait for change and go back to the beginning.
3. Use the route ID provided to communicate with the app.
 * If error, go back to the beginning.
4. Complete the communication.
5. Free the route id.

## Creating a Private Route

A Veilid app can create a private route with new_private_route().  This will, using default encryption,  creates a private route blob, which the application must then save to the DHT. As part of this functionality, the creating application must then consume RouteChanged events referencing that RouteID.  When triggered, the application must create a new private route and write it out to the same DHT entry and subkey as it wrote the prior route to. It keeps doing this until it's ready to shut down. Then it can write an empty vector to the DHT so there's no blob for a client to fail importing.

In Rust, the new_private_route stands up a routeid and blob for storage.

```
pub async fn newPrivateRoute() -> APIResult<VeilidRouteBlob> {
    let veilid_api = get_veilid_api()?;

    let (route_id, blob) = veilid_api.new_private_route().await?;

    let route_blob = VeilidRouteBlob { route_id, blob };
    APIResult::Ok(route_blob)
}
```

In Python, creating a new private route uses the JSON API, which is recommended for applications that use JSON rather than native code.  It looks something like this:

```
async with api:
    routeid, blob = await api.new_private_route()
```

You can see in that simple example that the function returns a route id, and a blob to store a message.  All of this is stored in the DHT which is hidden complexity. 

## Consuming a private route

The consuming app needs to get the route blob from the DHT, import it, fail on error, send to the routeID that is returned from import_private_route, and watch its callbacks for RouteChanged notifications that reference that routeID. When they happen, the procedure is to suspend sending until it gets a new non-erroring import, get the new route blob from the DHT, import it, and either loop/fail on error or use the new routeID that is returned on success. It keeps doing this until its communication is finished, then it frees its local resources by deleting the imported routeID.

```
async with api:
    async with routingcontext:
            privateroute = await api.import_remote_private_route(blob)
            message = b"Hello world!"
            await routingcontext.app_message(privateroute, message)
```

## More information

There is a great example of using private routing in the Python API unit tests, found here:

https://gitlab.com/veilid/veilid/-/blob/main/veilid-python/tests/test_routing_context.py

In the near future, the new_pivate_route() method will save a blob to the DHT that can then be collected by the consuming app and used to address messages back to the creator, thus reducing the load on the app developer.
