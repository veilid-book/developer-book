# Rust veilid-core crate functions

The veilid-core crate exposes a few functions that aren't attached to any particular type. These functions are generally helpful tools to work with common kinds of data one expects Veilid to work with, and two functions related to starting up the Veilid node.

| function name | What it does                                  | Returns    |
| ------------- | --------------------------------------------- | ---------- |
| api_startup   | Initialize a Veilid node.                     | VeilidAPI  |
| api_startup_json | Initialize a Veilid node, with the configuration in JSON format | |
| best_crypto_kind | Return the best cryptosystem kind we support | |
| best_envelope_version | Return the best envelope version we support | |
| common_crypto_kinds | Intersection of crypto kind vectors | |
| compare_crypto_kind | Sort best crypto kinds ‘less’, ordered toward the front of a list | |
| compress_prepend_size | | |
| decompress_size_prepended | | |
| deserialize_json | | |
| deserialize_json_bytes | | |
| deserialize_opt_json | | |
| deserialize_opt_json_bytes | | |
| get_aligned_timestamp | | |
| print_data | | |
| serialize_json | Serializes any Veilid object as JSON. | A JSON string |
| serialize_json_bytes | Serializes any Veilid object JSON. | Bytes containing a JSON string |
| veilid_version | Return the cargo package version of veilid-core in tuple format | |
| veilid_version_string | Return the cargo package version of veilid-core in string format | |
| vld0_generate_keypair | Generates a VLD0-scheme keypair | |
