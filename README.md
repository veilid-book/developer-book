# Veilid Developer Handbook

Work in progress book is available here:

https://veilid.gitlab.io/developer-book/

## Introduction

Veilid is both a protocol and an application development framework. 

Applications built using the Veilid protocol join a distributed network of other apps which all help each other by providing a distributed hash table which is E2EE and routed in a way which protects original node identity. 

The protocol and framework are both still in a beta-like state, not ready for production grade apps. 

We have an exemplar app called VeilidChat which is exactly what it sounds like. VeilidChat shows the power of the framework on Windows, MacOS, Android, iOS, and web applications. We hope to be able to open the beta test for VeilidChat in the coming months.

The purpose of this book is to provide guidance to developers that are consumers of the Veilid API.

Topics include:

* How Veilid works at a high level
* How to use `veilid-core` in your own apps

## Other Books

For documentation about the internals of Veilid and how it achieves what it does, see the _Veilid Internals Guide_.


## Contributing

The format of this book is mdBook, see here for how to contribute:

https://rust-lang.github.io/mdBook/index.html