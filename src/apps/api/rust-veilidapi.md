# VeilidAPI object

The VeilidAPI object is the root of all interaction with the Veilid node instance which the API accesses. To obtain the VeilidAPI object, call either `api_startup` or `api_startup_json` For more information, see the [Instantiating Veilid](apps/api/instantiating.md) section.

From [VeilidAPI], the following can be accessed:

| Subsystem Name | Description                                                              | ObjectType           |
| -------------- | ------------------------------------------------------------------------ | -------------------- |
| VeilidConfig   | The Veilid configuration specified at startup time                       | [[VeilidConfig]]     |
| Crypto         | The available set of cryptosystems provided by Veilid                    | [[Crypto]]           |
| TableStore     | The Veilid table-based encrypted persistent key-value store              | [[TableStore]]       |
| ProtectedStore | The Veilid abstract of the device's low-level 'protected secret storage' | [[ProtectedStore]]   |
| VeilidState    | The current state of the Veilid node this API accesses                   | [[VeilidState]]      |
| RoutingContext | Communication methods between Veilid nodes and private routes            | [[RoutingContext]]   |

In addition, the following functions can be performed:

- Reply to `AppCall` RPCs
- Attach and detach from the network
- Create and import private routes

## VeilidAPI functional surface

VeilidAPI exposes the following functions:

| function | What it does |
| -------- | ------------ |
| is_shutdown | Returns a boolean to indicate whether the API has been shut down. |
| shutdown | Shuts down the local instance in an orderly manner. |
| config   | Returns a reference to the running context's configuration. |
| crypto   | Returns a reference to the object which manages the running context's cryptography |
| table_store | Returns a reference to the DHT manager              |
| protected_store | Returns a reference to the local encrypted data store |
| get_state | Gets a full copy of the current state of Veilid. |
| attach   | Connects to the Veilid network. |
| detach   | Disconnects from the Veilid network. |
| routing_context | Get a new RoutingContext object to use to send messages over the network. |
| parse_as_target | Tries to parse a string as a RoutingContext target object. |
| new_private_route | Returns a newly allocated route with default crypto and network options. |
| new_private_route_custom | Like above, but lets you specify custom crypto, stability, and sequence options. |
| import_remote_private_route | Import a private route created by another Veilid node. |
| release_private_route | Deactivate and release a locally-created or imported private route. |
| app_call_reply | Replies to a particular incoming AppCall by ID. Answer size <= 32768 bytes. |

pub async fn shutdown(self)
pub fn is_shutdown(&self) -> bool
pub fn config(&self) -> VeilidAPIResult<VeilidConfig>
pub fn crypto(&self) -> VeilidAPIResult<Crypto>
pub fn table_store(&self) -> VeilidAPIResult<TableStore>
pub fn protected_store(&self) -> VeilidAPIResult<ProtectedStore>
pub async fn get_state(&self) -> VeilidAPIResult<VeilidState>
pub async fn attach(&self) -> VeilidAPIResult<()>
pub async fn detach(&self) -> VeilidAPIResult<()>
pub fn routing_context(&self) -> RoutingContext
pub async fn parse_as_target<S: AsRef<str>>(&self, s: S) -> VeilidAPIResult<Target>
pub async fn new_private_route(&self) -> VeilidAPIResult<(RouteId, Vec<u8>)>
pub async fn new_custom_private_route(&self, crypto_kinds: &[CryptoKind], stability: Stability, sequencing: Sequencing,)
pub fn import_remote_private_route(&self, blob: Vec<u8>) -> VeilidAPIResult<RouteId>
pub fn release_private_route(&self, route_id: RouteId) -> VeilidAPIResult<()>
pub async fn app_call_reply(&self, call_id: OperationId, message: Vec<u8>,)


