# Veilid Command Line Interface

The command like interface for Veilid, or veilid-cli, is a basic management console for headless Veilid nodes.  At the most basic, it will make sure your node is processing traffic, but there is so much more.

## Getting it running

Log into the endpoint that is hosting your node.  If you do not yet have your own node, check the installation guide to get started there, and then come back here.

From the prompt on your endpoint server, run the following command:

`veilid-cli [OPTIONS] [LOG LEVEL]`

This will bring up the ASCII user interface. Log level can include `debug` or `trace`.  Options include the following:

| Option | Description |
| ------ | ------ |
| `--address <ADDRESS>` | The address of the Veilid node to connect to.  This can be in i.p.v.4:port or [i:p:v:6::addr]:port format. |
| `--wait-for-debug` | Wait for the debugger to attach. |
| `-c, --config-file` | Specify a particular configuration file to use when launching. |
| `-h, --help` | Primary help listing. |
| `-v --version` | Print the version. |
| `-l` | Display just the server log |

 - If you hit 'e','w','d','i','t' using -l you can change the log level for the api from the terminal.
 - When using -l, the API log level can be changed interactively by pressing 'e' (error), 'w' (warning), 'i' (informational), 'd' (debug), and 't' (trace). Other log types cannot be changed from the terminal in this mode.
 - When not using -l, log levels for all log types can be changed with the 'change_log_level' command.


## The veilid-cli interface

![image of veilid-cli interface window](images/veilid-cli.png)

The top part of the application is the output pane.  Commands run in the cli will output to the output pane.

Directly below that is the collection of other nodes passing data through the connected node.  This is what shows what is happening, for what it's worth. Of course, no data from the messages is shown or available because of the encryption stack.  To sort by NodeId, Address, Ping, Downstream bandwith, or Upstream bandwidth, click the brackets next to the column lable.

Below the list of attached nodes is the Command field. This is where information and management commands can be issued, and the results will be shown in the output pane.  Click there first before beginning to type the commands.

## The veilid-cli commands

To start off, type `help` in the Command field. The onboard documentation is quite good, and will get you started.  The commands available are as follows:

| Command | Description |
| ------ | ------ |
| `exit` or `quit` | Exit the veilid-cli |
| `disconnect` | Disconnect the veilid-cli from the Veilid node |
| `shutdown` | Shut the server down |
| `change_log_level <layer> <level>` | Change the log level for a tracing layer. <br />Layers include `all`, `terminal`, `system`, `api`, `file`, `otlp`<br />Levels include: `error`, `warn`, `info`, `debug`, `trace` |
| enable [flag] | Set a flag\* |
| disable [flag] | Unset a flag\* |

\* The only flag currently enabled is app_message, which logs veilid-cli API connections with CommandProcessor.  Logging specifics are a runtime option, listed above.
