# Routing Contexts

A RoutingContext is how a node talks to other nodes.

Once a Veilid node is attached, it receives the fundamental routing information for the network. However, this doesn't allow for anonymity for either a sender or receiver, or for other communications preferences to be set. As such, a RoutingContext needs to be created.

A RoutingContext can be either "direct" or "with safety". If safety is selected, it will route messages via a safety route.

All RoutingContexts have a variable preference for Sequencing: Ensure ordered messaging, prefer unordered messaging, or have no
preference about ordered or unordered messaging. The default is to have no preference. In the "direct" RoutingContext case, there are no other attributes which can be or need to be set.

A RoutingContext with safety has 3 additional attributes:

1. An optional preferred beginning for the route, in the form of a RouteID.
2. Hop count: How many extra hops to create the route for. It must be greater than 0, and the default is 1.
3. Stability: Prefer either low latency or long uptime. The default is to prefer low latency.

It is possible to have multiple RoutingContext objects simultaneously, with different preferences.
