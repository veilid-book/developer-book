# Callback Messages

The update callback function receives ALL of the events that are relevant to the Veilid network and the local node. It is the application's job to filter them and decide what to respond to. This callback will receive messages of the following forms.

## The various update types

### Log

`Log`

This is a log message from the Veilid API. If the configuration does not specify the log level, it defaults to Error. Only messages with a value less than or equal to the configuration's log level will be sent. It has the following members:

- `level`: 1 byte which expresses how important the message is (lower values are more important), with the following meanings:

| Description | Value |
| ----------- | ------|
| Error       | 1     |
| Warn        | 2     |
| Info        | 3     |
| Debug       | 4     |
| Trace       | 5     |

- `message`: The log string.
- `backtrace`: An optional backtrace (in the veilid-core code) of where this log line was created.

### AppMessage

`AppMessage`

This is a message sent to the application running on this node, with no expectation of a response.

Veilid does no interpretation or modification of the AppMessage bytes sent.

It has the following members:

- `sender`: An optional node public key for what node sent this message.
- `message`: A `Vec<u8>` containing the message as sent by the sender.

### AppCall

`AppCall`

This is a remote procedure call to a procedure which the sender thinks is hosted by this application, with a response solicited. The receiver can safely ignore this message if it does not want to reply.

Veilid does no interpretation or modification of the AppCall bytes sent.

It has the following members:

- `sender`: An optional node public key for what node made this procedure call.
- `message`: A `Vec<u8>` containing the call and parameters as sent by the sender.
- `OperationID`: A value which must be passed to `app_call_reply` to indicate which appcall is being responded to.

If an AppCall is not responded to, it will time out, and the sender must choose whether or not to re-send it. The local Veilid node tracks OperationIDs that have not yet timed out, and will clean them up if they're not responded to.

### Attachment

`Attachment`

Regardless of what other systems refer to as "attachments", this is a notification of how well this Veilid node is attached to the network. Conceptually, it provides a "signal bar" mechanism for ease of understanding, based on the number of recent peers it has seen. The numbers given in the table are defaults, and can be set in the node configuration file.

| State Constant    | Value | Meaning                                        |
| ----------------- | ----- | ---------------------------------------------- |
| Detached          |   0   | Not connected, not trying to connect.          |
| Attaching         |   1   | Not connected, trying to connect.              |
| AttachedWeak      |   2   | 4 connections.                                 |
| AttachedGood      |   3   | 8 connections.                                 |
| AttachedStrong    |   4   | 16 connections.                                |
| FullyAttached     |   5   | 32 connections.                                |
| OverAttached      |   6   | 64 or more connections.                        |
| Detaching         |   7   | Connected, actively shutting connections down. |

The `Attachment` message has the following members:

- `state`: One of the above values, describing how well the node is currently attached to the Veilid network.
- `public_internet_ready`: A bool stating whether we are attached over the public internet, and if our network class and dial info have been determined.
- `local_network_ready`: A bool stating whether we are listening on the local network.

### Network

`Network`

This message conveys changes in the node's state, regarding network connectivity. It provides statistics for the whole node, not just any particular network interface that the node may be using.

This message has the following members:

- `started`: A boolean which indicates if the network has started.
- `bps_down`: The bytes per second (bps) which have been received.
- `bps_up`: The bytes per second (bps) which have been sent.
- `peers`: A `Vec<PeerTableData>` which contains a list of all recently connected peers.

Because `PeerTableData` is a complicated structure, it will be discussed later. [[ Note to self: WRITE ABOUT IT ]]

### Config

`Config`

This message conveys changes in the node's states, regarding its running configuration. It provides a single member, which contains a copy of the running configuration.

- `config`: A `VeilidConfigInner` object which provides information about the new current configuration.

Because `VeilidConfigInner` is a complicated structure, it will be discussed later. [[ Note to self: WRITE ABOUT IT ]]

### RouteChange

`RouteChange`

This message is sent when dead routes are detected on the network. It contains the following members:

- `dead_routes`: A `Vec<RouteId>` of locally allocated routes (safety routes and allocated private routes) which are no longer directly accessible. These have already been deallocated by the time this message is sent.
- `dead_remote_routes`: A `Vec<RouteId>` of routes which are no longer accessible on the network. This may include private routes which have been received from other nodes.

### ValueChange

`ValueChange`

(As of v0.2.4, this message is defined, but the functionality to request it is not available.)

When a DHT entry that this node has set a Watch on is updated, this message is sent.

The DHT is a topic of its own, and will be discussed in its own section. [[ Note to self: WRITE ABOUT IT ]]

This message contains the following members:

- `key`: The key of the DHT entry
- `subkeys`: A `Vec<ValueSubkey>` of the DHT entries that were updated.
- `count`: The number of updated DHT subkeys in the `subkeys` vector.
- `value`: A `ValueData` structure containing the update sequence number, the value, and the writer.

### Shutdown

`Shutdown`

When the node is shutting down and will no longer accept requests or send updates, this message will be sent. It has no members.
