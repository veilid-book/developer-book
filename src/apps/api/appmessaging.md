# AppCall and AppMessage

If you want to message an app directly via RPC, without necessarily having to wait for the DHT to make your changes available to your target, AppCall and AppMessage are where you should look.

It is very important to recognize this: Veilid does no interpretation or checking of the bytes in the AppCallQ, AppCallA, and AppMessage packets. You can truly shoot yourself in the foot here by accepting events for calls and messages which have not been authorized in some manner within your application. How you do so is entirely up to you. (Ideally, when the Veilid Standard Library comes out, it will have one or several mechanisms you will be able to use.)

AppCall is a query to a remote application, with an expected reply. AppMessage is a statement to a remote application, without an expected reply. Or, put another way, AppMessage is a call to a function that doesn't return anything.

## Replying to an AppCall

To reply to an AppCall, the method `VeilidAPI::app_call_reply` can be used. This uses the `OperationID` member of the AppCall event.

## Sending an AppCall or AppMessage

To send an AppCall or AppMessage, you must have a RoutingContext, typically obtained via a call to routing_context() or by cloning an existing RoutingContext.

You must also have a target NodeID.



